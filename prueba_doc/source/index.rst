.. doc-adsib documentation master file, created by
   sphinx-quickstart on Wed May 11 11:46:58 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to doc-adsib's documentation!
=====================================
Contents:

.. toctree::
   :maxdepth: 1

   doc_rst/cargo
   doc_rst/autenticacion


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
