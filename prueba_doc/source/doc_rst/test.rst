
Guia de Testing
===============

Para la elaboración de los Test se utlizo `Mocha`_

Para ejecutar los test, se escribe en la terminal el comando ``npm test`` dentro de la ruta donde está el proyecto.

Los procesos de cada modulo se encuentra en ``[entidad].controller.js``.

Los test de cada proceso se encuentra en ``[entidad].integration.js`` .

Primeramente se verifica si existe las rutas de cada entidad, todas esas verificaciones esta guardadas en ``index.spec.js`` de cada entidad.

Tome como ejemplo la creación de un postulante.

*Extraido de* ``server/api/postulantes/index.spec.js``

.. code:: javascript

      describe('POST /api/postulantes', function() {
        it('Debe apuntar a la ruta postulante.controller.create', function() {
          expect(routerStub.post
            .withArgs('/', 'postulanteCtrl.create')
            ).to.have.been.calledOnce;
        });
      });

Luego se procede a hacer los test en este ejemplo verificara si creo un postulante:

*Extraido de* ``server/api/postulacion/postulante.integration.js``

.. code:: javascript

      describe('POST /api/postulantes', function() {
        beforeEach(function(done) {
          request(app)
            .post('/api/postulantes')
            .send({
              nombres: 'Alvaro',
              apellidos: 'Alvarez Alvarado',
              ci: '123456 LP',
              sexo: true
            })
            .expect(201)
            .expect('Content-Type', /json/)
            .end((err, res) => {
              if (err) {
                return done(err);
              }
              nuevoPostulante = res.body;
              done();
            });
        });

        it('Debe responder con una nueva creacion de postulante', function() {
          expect(nuevoPostulante.nombres).to.equal('Alvaro');
          expect(nuevoPostulante.apellidos).to.equal('Alvarez Alvarado');
          expect(nuevoPostulante.ci).to.equal('123456 LP');
          expect(nuevoPostulante.sexo).to.equal(true);
        });
      });

En la función ``beforeEach()`` se envia los datos del postulante a la ruta ``/api/postulantes`` y nos devuelve un json en ``res.body``.

En la función ``it()`` se compara si los datos recibidos son iguales a los enviados.

En algunos casos particulares como ser "Obtener las postulaciones de un determinado postulante".

.. code:: javascript

  describe('GET /api/postulaciones/postulante/:id',function(){
      var postulaciones;
      beforeEach(function(done){
        request(app)
        .get('/api/postulaciones/postulante/'+nuevoPostulante._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          postulaciones = res.body;
          done();
      })
    })
    it('responder con 200 si existe los cargos',function(){
      expect(postulaciones[0]._id).to.equal(nuevoCargo._id);
      expect(postulaciones[0].nombre).to.equal(nuevoCargo.nombre);
      expect(postulaciones[0].Postulacion._id).to.equal(nuevoPostulacion._id);
    });
  })

La tabla "postulacion" esta relacionada con las tablas "postulante" y "cargo", era necesario tener los datos del postulante y cargo en la base de datos. Previamente se enviaron los datos del postulante y cargo.


.. _Mocha: https://mochajs.org/
