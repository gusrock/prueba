Mostrar todos los Tipos de Dato
===============================

URL
---
::

  /tiposDato

Método
------
``GET``

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "EMAIL",
        "nombre": "correo electronico",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "TEL",
        "nombre": "Telefono",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "WEB",
        "nombre": "Pagina Web",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Parámetros opcionales de la URL
-------------------------------

>**Muestra una determinada cantidad de tipos de dato
`````````````````````````````````````````````````````````````

**URL**
::

  /tiposDato?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /tiposDato?elementos=2

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "EMAIL",
        "nombre": "correo electronico",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "TEL",
        "nombre": "Telefono",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Muestra los tipos de dato en una determinada página
```````````````````````````````````````````````````

**URL**
::

  /tiposDato?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /tiposDato?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "EMAIL",
        "nombre": "correo electronico",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "TEL",
        "nombre": "Telefono",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "WEB",
        "nombre": "Pagina Web",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Ordena los tipos de dato ascendentemente en base a un campo del modelo
``````````````````````````````````````````````````````````````````````

**URL**
::

  /tiposDato?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /tiposDato?ordenarPor=nombre

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "EMAIL",
        "nombre": "correo electronico",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "WEB",
        "nombre": "Pagina Web",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "TEL",
        "nombre": "Telefono",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Ordena los tipos de dato descendentemente en base a un campo del modelo
```````````````````````````````````````````````````````````````````````

**URL**
::

  /tiposDato?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /tiposDato?ordenarPor=nombre&orden=true

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "TEL",
        "nombre": "Telefono",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "WEB",
        "nombre": "Pagina Web",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      },
      {
        "propiedad": "EMAIL",
        "nombre": "correo electronico",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Busca los Tipos De Datos que contengas las palabras "TEL" y "Telefono" en los campos "propiedad" y "nombre"
```````````````````````````````````````````````````````````````````````````````````````````````````````````

**URL**
::

  /tiposDato?en=valor1&en=valor2&palabras=valor3&palabras=valor4

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo:**
::

  /tiposDato?en=propiedad&en=nombres&palabras=TEL&palabras=Telefono

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "TEL",
        "nombre": "Telefono",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Muestra los postulantes de tienen la propiedad "EMAIL"
``````````````````````````````````````````````````````

**URL**
::

  /tiposDato?en=valor1&incluye=%7B%22entidad%22:%22valor2%22%7D&palabras=valor3

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
`palabra`     String (array)  Cada elemento del array contiene `palabra` que representa el criterio a buscar.
============  ==============  ================================================================================

**Ejemplo:**
::

  /tiposDato?en=propiedad&incluye=%7B%22entidad%22:%22Postulantes%22%7D&palabras=EMAIL

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "propiedad": "EMAIL",
        "nombre": "correo electronico",
        "createdAt": "2016-03-14T22:52:31.554Z",
        "updatedAt": "2016-03-14T22:52:31.554Z",
        "Postulantes":
        [
          {
            "_id": 50,
            "nombres": "Fabio",
            "apellidos": "Gonzales Lima",
            "ci": 37234258,
            "fecha_creacion": "2016-01-14T22:52:31.554Z",
            "fecha_modificacion": "2016-01-14T22:52:31.554Z",
            "Datos":
            {
              "_id": 2,
              "valor": "fabio@gmail.com",
              "createdAt": "2016-02-14T22:03:48.216Z",
              "updatedAt": "2016-02-14T22:03:48.216Z",
              "fk_postulante": 50,
              "propiedad": "EMAIL"
            }
          },
          {
            "_id": 60,
            "nombres": "Hugo",
            "apellidos": "Molina Mendez",
            "ci": 39398,
            "fecha_creacion": "2016-01-14T22:52:31.554Z",
            "fecha_modificacion": "2016-01-14T22:52:31.554Z",
            "Datos":
            {
              "_id": 2,
              "valor": "hmm@gmail.com",
              "createdAt": "2016-02-14T22:03:48.216Z",
              "updatedAt": "2016-02-14T22:03:48.216Z",
              "fk_postulante": 60,
              "propiedad": "EMAIL"
            }
          }
        ]
      }
    ]
  }

Respuesta de error
------------------

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
-------------
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 tipos de dato por defecto

----

Crear Tipo de Dato
==================

URL
---
::

  /tiposDato

Método
------
``POST``

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
    "propiedad": "WEB",
    "nombre": "Página"
  }

Respuesta correcta
------------------

**Código:** 201

**Contenido:**

.. code:: json

  {
    "propiedad": "WEB",
    "nombre": "Página",
    "createdAt": "2016-03-14T22:52:31.554Z",
    "updatedAt": "2016-03-14T22:52:31.554Z"
  }

Respuesta de error
------------------

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un nombre",
    "errors": [
      {
        "message": "Por favor ingrese un nombre",
        "type": "Validation error",
        "path": "nombre",
        "value": {},
        "__raw": {}
      }
    ]
  }

**Código:** 409

**Contenido:**

.. code:: json

  {
    "name": "SequelizeUniqueConstraintError",
    "message": "El objeto ya existe",
    "errors": {}
  }

----

Mostrar un Tipo de dato
========================

URL
---
::

  /tiposDato/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          String          Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=EMAIL

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "propiedad": "EMAIL",
    "nombre": "correo electronico",
    "createdAt": "2016-03-14T22:52:31.554Z",
    "updatedAt": "2016-03-14T22:52:31.554Z"
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar Tipo de Dato
===================

URL
---
::

  /tiposDato

Método
------
``PUT``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          String          Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id="WEB"

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
    "propiedad": "WEB",
    "nombre": "Página web"
  }

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "propiedad": "WEB",
    "nombre": "Página web",
    "createdAt": "2016-03-14T22:52:31.554Z",
    "updatedAt": "2016-03-14T22:52:31.554Z"
  }

**Repuesta de error**

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un nombre",
    "errors": [
      {
        "message": "Por favor ingrese un nombre",
        "type": "Validation error",
        "path": "nombre",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar Tipo de Dato
=====================

URL
---
::

  /tiposDato/

Método
------
``DELETE``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          String          Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id="WEB"

Respuesta correcta
------------------

**Código:** 204

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
