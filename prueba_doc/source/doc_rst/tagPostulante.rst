Mostrar todas las relaciones entre tags y postulantes
=====================================================

URL
---
::

  /tagsPostulantes

Método
------
``GET``

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 63,
        "fecha_creacion": "2016-01-16T15:50:28.738Z",
        "fecha_modificacion": "2016-01-16T15:50:28.738Z",
        "fk_postulante": 250,
        "fk_tag": 190
      },
      {
        "_id": 64,
        "fecha_creacion": "2016-03-03T15:50:28.752Z",
        "fecha_modificacion": "2016-03-03T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188
      },
      {
        "_id": 65,
        "fecha_creacion": "2016-03-16T15:50:28.753Z",
        "fecha_modificacion": "2016-03-16T15:50:28.753Z",
        "fk_postulante": 248,
        "fk_tag": 189
      }
    ]
  }

Parámetros opcionales de la URL
-------------------------------

Muestra una determinada cantidad de relaciones entre un tag y un postulante
```````````````````````````````````````````````````````````````````````````

**URL**
::

  /tagsPostulantes?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /tagsPostulantes?elementos=2

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 63,
        "fecha_creacion": "2016-01-16T15:50:28.738Z",
        "fecha_modificacion": "2016-01-16T15:50:28.738Z",
        "fk_postulante": 250,
        "fk_tag": 190
      },
      {
        "_id": 64,
        "fecha_creacion": "2016-03-03T15:50:28.752Z",
        "fecha_modificacion": "2016-03-03T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188
      }
    ]
  }

Muestra las relaciones entre un tag y un postulante en una determinada página
`````````````````````````````````````````````````````````````````````````````

**URL**
::

  /tagsPostulantes?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /tagsPostulantes?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 63,
        "fecha_creacion": "2016-01-16T15:50:28.738Z",
        "fecha_modificacion": "2016-01-16T15:50:28.738Z",
        "fk_postulante": 250,
        "fk_tag": 190
      },
      {
        "_id": 64,
        "fecha_creacion": "2016-03-03T15:50:28.752Z",
        "fecha_modificacion": "2016-03-03T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188
      },
      {
        "_id": 65,
        "fecha_creacion": "2016-03-16T15:50:28.753Z",
        "fecha_modificacion": "2016-03-16T15:50:28.753Z",
        "fk_postulante": 248,
        "fk_tag": 189
      }
    ]
  }

Ordena las relaciones entre un tag y un postulante ascendentemente en base a un campo del modelo
````````````````````````````````````````````````````````````````````````````````````````````````

**URL**
::

  /tagsPostulantes?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /tagsPostulantes?ordernarPor=fk_postulante

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 64,
        "fecha_creacion": "2016-03-03T15:50:28.752Z",
        "fecha_modificacion": "2016-03-03T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188
      },
      {
        "_id": 65,
        "fecha_creacion": "2016-03-16T15:50:28.753Z",
        "fecha_modificacion": "2016-03-16T15:50:28.753Z",
        "fk_postulante": 248,
        "fk_tag": 189
      },
      {
        "_id": 63,
        "fecha_creacion": "2016-01-16T15:50:28.738Z",
        "fecha_modificacion": "2016-01-16T15:50:28.738Z",
        "fk_postulante": 250,
        "fk_tag": 190
      }
    ]
  }

Ordena las relaciones entre un tag y un postulante descendentemente en base a un campo del modelo
`````````````````````````````````````````````````````````````````````````````````````````````````

**URL**
::

  /tagsPostulantes?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /tagsPostulantes?ordenarPor=fk_postulante&orden=true

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 63,
        "fecha_creacion": "2016-01-16T15:50:28.738Z",
        "fecha_modificacion": "2016-01-16T15:50:28.738Z",
        "fk_postulante": 250,
        "fk_tag": 190
      },
      {
        "_id": 65,
        "fecha_creacion": "2016-03-16T15:50:28.753Z",
        "fecha_modificacion": "2016-03-16T15:50:28.753Z",
        "fk_postulante": 248,
        "fk_tag": 189
      },
      {
        "_id": 64,
        "fecha_creacion": "2016-03-03T15:50:28.752Z",
        "fecha_modificacion": "2016-03-03T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188
      }
    ]
  }

Muestra los detalles de Postulantes y Tag
`````````````````````````````````````````

**URL**
::

  /tagsPostulantes?incluye=%7B%22entidad%22:%22valor1%22%7D&incluye=%7B%22entidad%22:%22valor2%22%7D

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
============  ==============  ================================================================================

**Ejemplo:**
::

  /tagsPostulantes?incluye=%7B%22entidad%22:%22Tag%22%7D&incluye=%7B%22entidad%22:%22Postulante%22%7D

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 63,
        "fecha_creacion": "2016-01-16T15:50:28.738Z",
        "fecha_modificacion": "2016-01-16T15:50:28.738Z",
        "fk_postulante": 250,
        "fk_tag": 190,
        "Postulante":
        {
          "_id": 250,
          "nombres": "Juan",
          "apellidos": "Garzofino Blanco",
          "ci": 172348,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Tag":
        {
          "_id": 190,
          "nombre": "Java",
          "fecha_creacion": "2016-03-16T15:50:28.629Z",
          "fecha_modificacion": "2016-03-16T15:50:28.629Z"
        }
      },
      {
        "_id": 64,
        "fecha_creacion": "2016-03-03T15:50:28.752Z",
        "fecha_modificacion": "2016-03-03T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188,
        "Postulante":
        {
          "_id": 247,
          "nombres": "Daniel Hugo",
          "apellidos": "Estrada Jimenez",
          "ci": 27238,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Tag":
        {
          "_id": 188,
          "nombre": "Python",
          "fecha_creacion": "2016-03-16T15:50:28.629Z",
          "fecha_modificacion": "2016-03-16T15:50:28.629Z"
        }
      },
      {
        "_id": 65,
        "fecha_creacion": "2016-03-16T15:50:28.753Z",
        "fecha_modificacion": "2016-03-16T15:50:28.753Z",
        "fk_postulante": 248,
        "fk_tag": 189,
        "Postulante":
        {
          "_id": 248,
          "nombres": "Alba",
          "apellidos": "Guzman Arias",
          "ci": 4542,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Tag":
        {
          "_id": 189,
          "nombre": "PHP",
          "fecha_creacion": "2016-03-16T15:50:28.629Z",
          "fecha_modificacion": "2016-03-16T15:50:28.629Z"
        }
      }
    ]
  }

Respuesta de error
------------------

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
-------------
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 relaciones entre Tag y postulante por defecto
- Solo se puede hacer "incluye" con las entidades "tag" y "postulante"

----

Crear una relacion entre un tag y un postulante
===============================================

URL
---
::

  /tagsPostulantes

Método
------
``POST``

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
      "fk_postulante":246,
      "fk_tag":187
  }

Respuesta correcta
------------------

**Código:** 201

**Ejemplo:**

.. code:: json

  {
    "_id": 68,
    "fk_postulante": 246,
    "fk_tag": 187,
    "fecha_modificacion": "2016-03-16T19:01:06.559Z",
    "fecha_creacion": "2016-03-16T19:01:06.559Z"
  }

Respuesta de error
------------------

**Código:** 500

**Contenido:**

.. code:: json

  {
    "name": "SequelizeForeignKeyConstraintError",
    "message": "inserción o actualización en la tabla «tag_postulante» viola la llave foránea «tag_postulante_fk_tag_fkey»"
  }

----

Mostrar una relacion entre un tag y un postulante
=================================================

URL
---
::

  /tagsPostulantes/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=63

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 63,
    "fecha_creacion": "2016-03-16T15:50:28.738Z",
    "fecha_modificacion": "2016-03-16T15:50:28.738Z",
    "fk_postulante": 246,
    "fk_tag": 187
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar una relacion entre tag y postulante
==========================================

URL
---
::

  /tagsPostulantes/:id

Método
------
``PUT``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=63

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
      "fk_postulante":246,
      "fk_tag":188
  }

Respuesta correcta
------------------

**Código:** 200

**Ejemplo:**

.. code:: json

  {
    "_id": 63,
    "fecha_creacion": "2016-03-16T15:50:28.738Z",
    "fecha_modificacion": "2016-03-16T19:07:58.475Z",
    "fk_postulante": 246,
    "fk_tag": 188
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Eliminar una relacion entre un tag y un postulante
==================================================

URL
---
::

  /tagsPostulantes/:id

Método
------
``DELETE``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=63

Respuesta correcta
------------------

**Código:** 204

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


Obtener todos los tags de un postulante
=======================================

URL
---
::

  /tagsPostulantes/postulante/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  [
    {
      "_id": 188,
      "nombre": "tag 2",
      "fecha_creacion": "2016-03-16T15:50:28.628Z",
      "fecha_modificacion": "2016-03-16T15:50:28.628Z",
      "TagPostulante": {
        "_id": 64,
        "fecha_creacion": "2016-03-16T15:50:28.752Z",
        "fecha_modificacion": "2016-03-16T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 188
      }
    },
    {
      "_id": 189,
      "nombre": "tag 4",
      "fecha_creacion": "2016-03-16T15:50:28.628Z",
      "fecha_modificacion": "2016-03-16T15:50:28.628Z",
      "TagPostulante": {
        "_id": 65,
        "fecha_creacion": "2016-03-16T15:50:28.752Z",
        "fecha_modificacion": "2016-03-16T15:50:28.752Z",
        "fk_postulante": 247,
        "fk_tag": 189
      }
    }
  ]

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


Obtener todos los postulantes de un Tag
=======================================

URL
---
::

  /tagsPostulantes/tag/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=194

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  [
    {
      "_id": 251,
      "nombres": "Juan Carlos",
      "apellidos": "Perez Gomez",
      "ci": "6867562",
      "createdAt": "2016-03-16T19:18:07.801Z",
      "updatedAt": "2016-03-16T19:18:07.801Z",
      "TagPostulante": {
        "_id": 70,
        "fecha_creacion": "2016-03-16T19:18:08.004Z",
        "fecha_modificacion": "2016-03-16T19:18:08.004Z",
        "fk_postulante": 251,
        "fk_tag": 194
      }
    },
    {
      "_id": 252,
      "nombres": "Ana Maria",
      "apellidos": "Suarez",
      "ci": "6867122",
      "createdAt": "2016-03-16T19:18:07.801Z",
      "updatedAt": "2016-03-16T19:18:07.801Z",
      "TagPostulante": {
        "_id": 71,
        "fecha_creacion": "2016-03-16T19:18:08.004Z",
        "fecha_modificacion": "2016-03-16T19:18:08.004Z",
        "fk_postulante": 252,
        "fk_tag": 194
      }
    }
  ]

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
