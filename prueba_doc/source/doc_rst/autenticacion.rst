Autenticación de usuario
========================

URL
---
::

  /autenticar/ldap

Método
------
``POST``

Parámetros del body
-------------------

.. code:: json

  {
      "nombreUsuario": "test",
      "contrasena": "123456"
  }

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiJ0ZXN0IiwiY29ycmVvIjoidGVzdEBnbWFpbC5jb20iLCJub21icmVDb21wbGV0byI6IlRlc3QgR3V0aWVycmV6Iiwicm9sZXMiOlsiQURNSU5JU1RSQVRPUiJdLCJpYXQiOjE0NjAxMjQ2MTQsImV4cCI6MTQ2MDE0MjYxNH0.fZ401WElQgKmASLxV_vFaR2YfVJIapTbqBJeIsOxjbw"
  }

Respuesta incorrecta
--------------------

**Código:** 400

**Contenido**

.. code:: json

  {
    "message": "Invalid username/password"
  }

**Código:** 404

**Contenido**

.. code:: json

    {
      "message": "Missing credentials"
    }

Observaciones
=============

- Se debe añadir el token en todas las peticiones de la API
