Mockups Proyecto
================

**Introducción**

El mockup del Proyecto es un prototipo con el que proporciona al menos una parte de la funcionalidad del sistema y permite pruebas del diseño. Este diseño permitirá comentarios por parte de los usuarios.

**Software de diseño mockup - Pencil**

Pencil es una bonita herramienta GUI de prototipos de código abierto.

**Instalación Pencil en Debian Jessie**

- Descarga la ultima versión.

.. code:: sh

    $ wget https://evoluspencil.googlecode.com/files/evoluspencil_2.0.5_all.deb

- Descomprimir el paquete debian

.. code:: sh

    $ dpkg-deb -x evoluspencil_2.0.5_all.deb dir_tmp
    $ dpkg-deb --control evoluspencil_2.0.5_all.deb dir_tmp/DEBIAN

- Editar archivo dir\_tmp/DEBIAN/control

.. code:: sh

    Package: evoluspencil
    Version: 2.0.5
    Architecture: all
    Maintainer: Nguyen Hong Thai
    Installed-Size: 8952
    Depends: iceweasel
    Section: unknown
    Priority: extra
    Homepage: http://pencil-project.org/
    Description: Evolus Pencil
      Pencil is an open source GUI prototyping and sketching tools released under GPL.

- Vuelva a empaquetar el paquete debian

.. code:: sh

    $ dpkg -b dir_tmp evoluspencil_2.0.5_all.deb

- Instalar el paquete

.. code:: sh

    $ sudo dpkg -i evoluspencil_2.0.5_all.deb
