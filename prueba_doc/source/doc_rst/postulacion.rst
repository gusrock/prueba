Mostrar todas las postulaciones
===============================

URL
---
::

  /postulaciones

Método
------
``GET``

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 21,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:26:38.736Z",
        "fecha_modificacion": "2016-03-16T15:26:38.736Z",
        "fk_postulante": 236,
        "fk_cargo": 276
      },
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277
      },
      {
        "_id": 23,
        "presentacion": "Poseo las aptitudes necesarias para postular a este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.755Z",
        "fecha_modificacion": "2016-03-16T15:26:38.755Z",
        "fk_postulante": 238,
        "fk_cargo": 278
      },
      {
        "_id": 24,
        "presentacion": "Considero este cargo apto para mi",
        "fecha_creacion": "2016-03-16T15:26:38.756Z",
        "fecha_modificacion": "2016-03-16T15:26:38.756Z",
        "fk_postulante": 239,
        "fk_cargo": 279
      },
      {
        "_id": 25,
        "presentacion": "Dare lo mejor de mi si logro ocupar este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.774Z",
        "fecha_modificacion": "2016-03-16T15:26:38.774Z",
        "fk_postulante": 240,
        "fk_cargo": 280
      }
    ]
  }

Parámetros opcionales de la URL
-------------------------------

Muestra una determinada cantidad de postulaciones
`````````````````````````````````````````````````

**URL**
::

  /postulaciones?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /postulaciones?elementos=2

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 21,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:26:38.736Z",
        "fecha_modificacion": "2016-03-16T15:26:38.736Z",
        "fk_postulante": 236,
        "fk_cargo": 276
      },
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277
      }
    ]
  }

Muestra las postulaciones en una determinada página
```````````````````````````````````````````````````
**URL**
::

  /postulaciones?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /postulaciones?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 21,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:26:38.736Z",
        "fecha_modificacion": "2016-03-16T15:26:38.736Z",
        "fk_postulante": 236,
        "fk_cargo": 276
      },
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277
      },
      {
        "_id": 23,
        "presentacion": "Poseo las aptitudes necesarias para postular a este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.755Z",
        "fecha_modificacion": "2016-03-16T15:26:38.755Z",
        "fk_postulante": 238,
        "fk_cargo": 278
      },
      {
        "_id": 24,
        "presentacion": "Considero este cargo apto para mi",
        "fecha_creacion": "2016-03-16T15:26:38.756Z",
        "fecha_modificacion": "2016-03-16T15:26:38.756Z",
        "fk_postulante": 239,
        "fk_cargo": 279
      },
      {
        "_id": 25,
        "presentacion": "Dare lo mejor de mi si logro ocupar este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.774Z",
        "fecha_modificacion": "2016-03-16T15:26:38.774Z",
        "fk_postulante": 240,
        "fk_cargo": 280
      }
    ]
  }

Ordena las postulaciones ascendentemente en base a un campo del modelo
``````````````````````````````````````````````````````````````````````

**URL**
::

  /postulaciones?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /postulaciones?ordenarPor=presentacion

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 24,
        "presentacion": "Considero este cargo apto para mi",
        "fecha_creacion": "2016-03-16T15:26:38.756Z",
        "fecha_modificacion": "2016-03-16T15:26:38.756Z",
        "fk_postulante": 239,
        "fk_cargo": 279
      },
      {
        "_id": 25,
        "presentacion": "Dare lo mejor de mi si logro ocupar este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.774Z",
        "fecha_modificacion": "2016-03-16T15:26:38.774Z",
        "fk_postulante": 240,
        "fk_cargo": 280
      },
      {
        "_id": 23,
        "presentacion": "Poseo las aptitudes necesarias para postular a este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.755Z",
        "fecha_modificacion": "2016-03-16T15:26:38.755Z",
        "fk_postulante": 238,
        "fk_cargo": 278
      },
      {
        "_id": 21,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:26:38.736Z",
        "fecha_modificacion": "2016-03-16T15:26:38.736Z",
        "fk_postulante": 236,
        "fk_cargo": 276
      },
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277
      }
    ]
  }

Ordena las postulaciones descendentemente en base a un campo del modelo
```````````````````````````````````````````````````````````````````````

**URL**
::

  /postulaciones?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /postulaciones?ordenarPor=presentacion&orden=true

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277
      },
      {
        "_id": 21,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:26:38.736Z",
        "fecha_modificacion": "2016-03-16T15:26:38.736Z",
        "fk_postulante": 236,
        "fk_cargo": 276
      },    {
        "_id": 23,
        "presentacion": "Poseo las aptitudes necesarias para postular a este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.755Z",
        "fecha_modificacion": "2016-03-16T15:26:38.755Z",
        "fk_postulante": 238,
        "fk_cargo": 278
      },
      {
        "_id": 25,
        "presentacion": "Dare lo mejor de mi si logro ocupar este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.774Z",
        "fecha_modificacion": "2016-03-16T15:26:38.774Z",
        "fk_postulante": 240,
        "fk_cargo": 280
      },
      {
        "_id": 24,
        "presentacion": "Considero este cargo apto para mi",
        "fecha_creacion": "2016-03-16T15:26:38.756Z",
        "fecha_modificacion": "2016-03-16T15:26:38.756Z",
        "fk_postulante": 239,
        "fk_cargo": 279
      }
    ]
  }

Busca los postulantes que contenga en su presentación la palabra "apto"
```````````````````````````````````````````````````````````````````````

**URL**
::

  /postulaciones?en=valor1&palabras=valor2

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo:**
::

  /postulaciones?en=presentacion&palabras=apto

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277
      },
      {
        "_id": 24,
        "presentacion": "Considero este cargo apto para mi",
        "fecha_creacion": "2016-03-16T15:26:38.756Z",
        "fecha_modificacion": "2016-03-16T15:26:38.756Z",
        "fk_postulante": 239,
        "fk_cargo": 279
      }
    ]
  }

Muestra los postulantes y los cargos de cada postulacion
````````````````````````````````````````````````````````

**URL**
::

  /postulaciones?incluye=%7B%22entidad%22:%22valor1%22%7D&incluye=%7B%22entidad%22:%22valor2%22%7D

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
============  ==============  ================================================================================

**Ejemplo:**
::

  /postulaciones?incluye=%7B%22entidad%22:%22Postulante%22%7D&incluye=%7B%22entidad%22:%22Cargo%22%7D

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 21,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:26:38.736Z",
        "fecha_modificacion": "2016-03-16T15:26:38.736Z",
        "fk_postulante": 236,
        "fk_cargo": 276,
        "Postulante":
        {
          "_id": 236,
          "nombres": "Teodoro",
          "apellidos": "Alarcon Layme",
          "ci": 3732348,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Cargo":
        {
          "_id": 276,
          "nombre": "Secretario",
          "fecha_creacion": "2016-03-14T22:52:31.554Z",
          "fecha_modificacion": "2016-03-14T22:52:31.554Z"
        }
      },
      {
        "_id": 22,
        "presentacion": "Soy apto para este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.737Z",
        "fecha_modificacion": "2016-03-16T15:26:38.737Z",
        "fk_postulante": 237,
        "fk_cargo": 277,
        "Postulante":
        {
          "_id": 237,
          "nombres": "Kevin Alan",
          "apellidos": "Espinoza Layme",
          "ci": 31248,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Cargo":
        {
          "_id": 277,
          "nombre": "Ingeniero Civil",
          "fecha_creacion": "2016-03-14T22:52:31.554Z",
          "fecha_modificacion": "2016-03-14T22:52:31.554Z"
        }
      },
      {
        "_id": 23,
        "presentacion": "Poseo las aptitudes necesarias para postular a este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.755Z",
        "fecha_modificacion": "2016-03-16T15:26:38.755Z",
        "fk_postulante": 238,
        "fk_cargo": 278,
        "Postulante":
        {
          "_id": 238,
          "nombres": "Ximena Carla",
          "apellidos": "Ramirez Paredes",
          "ci": 1112,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Cargo":
        {
          "_id": 278,
          "nombre": "Contador",
          "fecha_creacion": "2016-03-14T22:52:31.554Z",
          "fecha_modificacion": "2016-03-14T22:52:31.554Z"
        }
      },
      {
        "_id": 24,
        "presentacion": "Considero este cargo apto para mi",
        "fecha_creacion": "2016-03-16T15:26:38.756Z",
        "fecha_modificacion": "2016-03-16T15:26:38.756Z",
        "fk_postulante": 239,
        "fk_cargo": 279,
        "Postulante":
        {
          "_id": 239,
          "nombres": "Maria",
          "apellidos": "Blanco Delgadillo",
          "ci": 975454,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Cargo":
        {
          "_id": 279,
          "nombre": "Jefe de Sistemas",
          "fecha_creacion": "2016-03-14T22:52:31.554Z",
          "fecha_modificacion": "2016-03-14T22:52:31.554Z"
        }
      },
      {
        "_id": 25,
        "presentacion": "Dare lo mejor de mi si logro ocupar este cargo",
        "fecha_creacion": "2016-03-16T15:26:38.774Z",
        "fecha_modificacion": "2016-03-16T15:26:38.774Z",
        "fk_postulante": 240,
        "fk_cargo": 280,
        "Postulante":
        {
          "_id": 240,
          "nombres": "Ariel Wilmer",
          "apellidos": "Perez Apaza",
          "ci": 123432,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "Cargo":
        {
          "_id": 280,
          "nombre": "Abogado",
          "fecha_creacion": "2016-03-14T22:52:31.554Z",
          "fecha_modificacion": "2016-03-14T22:52:31.554Z"
        }
      }
    ]
  }

Respuesta de error
------------------

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
-------------
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 postulaciones por defecto
- Solo se puede hacer "incluye" con las entidades "cargo" y "postulante"

----

Crear una postulacion
=====================

URL
---
::

  /postulaciones

Método
------
``POST``

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
      "presentacion":"Texto de presentacion del postulante",
      "fk_postulante":236,
      "fk_cargo":277
  }

Respuesta correcta
------------------

**Código:** 201

**Ejemplo:**

.. code:: json

  {
    "_id": 26,
    "presentacion": "Texto de presentacion del postulante",
    "fk_postulante": 236,
    "fk_cargo": 277,
    "fecha_modificacion": "2016-03-16T15:30:20.292Z",
    "fecha_creacion": "2016-03-16T15:30:20.292Z"
  }

Respuesta de error
------------------

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un mensaje de presentacion",
    "errors": [
      {
        "message": "Por favor ingrese un mensaje de presentacion",
        "type": "Validation error",
        "path": "presentacion",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Mostrar una postulación
=======================

URL
---
::

  /postulaciones/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=21

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 21,
    "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
    "fecha_creacion": "2016-03-16T15:26:38.736Z",
    "fecha_modificacion": "2016-03-16T15:26:38.736Z",
    "fk_postulante": 236,
    "fk_cargo": 276
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar una postulacion
======================

URL
---
::

  /postulaciones/:id

Método
------
``PUT``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
      "presentacion":"Texto de presentacion del postulante modificado",
      "fk_postulante":236,
      "fk_cargo":277
  }

Respuesta correcta
------------------

**Código:** 200

**Ejemplo:**

.. code:: json

  {
    "_id": 26,
    "presentacion": "Texto de presentacion del postulante modificado",
    "fecha_creacion": "2016-03-16T15:30:20.292Z",
    "fecha_modificacion": "2016-03-16T15:33:51.913Z",
    "fk_postulante": 236,
    "fk_cargo": 277
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un mensaje de presentacion",
    "errors": [
      {
        "message": "Por favor ingrese un mensaje de presentacion",
        "type": "Validation error",
        "path": "presentacion",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar una postulacion
========================

URL
---
::

  /postulaciones/:id

Método
------
``DELETE``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
------------------

**Código:** 204

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

Obtener todas las postulaciones de un postulante
================================================

URL
---
::

  /postulaciones/postulate/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  [
    {
      "_id": 286,
      "nombre": "Cargo1",
      "fecha_creacion": "2016-03-16T15:40:22.267Z",
      "fecha_modificacion": "2016-03-16T15:40:22.267Z",
      "Postulacion": {
        "_id": 28,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:40:22.382Z",
        "fecha_modificacion": "2016-03-16T15:40:22.382Z",
        "fk_postulante": 241,
        "fk_cargo": 286
      }
    },
    {
      "_id": 287,
      "nombre": "Cargo 2",
      "fecha_creacion": "2016-03-16T15:40:22.267Z",
      "fecha_modificacion": "2016-03-16T15:40:22.267Z",
      "Postulacion": {
        "_id": 29,
        "presentacion": "Texto de presentacion 2",
        "fecha_creacion": "2016-03-16T15:40:22.382Z",
        "fecha_modificacion": "2016-03-16T15:40:22.382Z",
        "fk_postulante": 241,
        "fk_cargo": 287
      }
    }
  ]

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


Obtener todos los postulantes a un cargo
========================================

URL
---
::

  /postulaciones/cargo/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  [
    {
      "_id": 241,
      "nombres": "Juan Carlos",
      "apellidos": "Perez Gomez",
      "ci": "6867562",
      "createdAt": "2016-03-16T15:40:22.195Z",
      "updatedAt": "2016-03-16T15:40:22.195Z",
      "Postulacion": {
        "_id": 28,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:40:22.382Z",
        "fecha_modificacion": "2016-03-16T15:40:22.382Z",
        "fk_postulante": 241,
        "fk_cargo": 286
      }
    },
    {
      "_id": 242,
      "nombres": "Ana Gomez",
      "apellidos": "Rodriguez",
      "ci": "6867122",
      "createdAt": "2016-03-16T15:40:22.195Z",
      "updatedAt": "2016-03-16T15:40:22.195Z",
      "Postulacion": {
        "_id": 29,
        "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
        "fecha_creacion": "2016-03-16T15:40:22.382Z",
        "fecha_modificacion": "2016-03-16T15:40:22.382Z",
        "fk_postulante": 242,
        "fk_cargo": 286
      }
    }
  ]

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
