Mostrar todos los Datos de Postulantes
======================================

URL
---
::

  /datosPostulantes

Método
------
``GET``

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 2,
        "valor": "daniel@daniel.com",
        "createdAt": "2016-02-14T22:03:48.216Z",
        "updatedAt": "2016-02-14T22:03:48.216Z",
        "fk_postulante": 1,
        "propiedad": "EMAIL"
      },
      {
        "_id": 3,
        "valor": "abc@gmail.com",
        "createdAt": "2016-03-16T22:08:55.216Z",
        "updatedAt": "2016-03-16T22:08:55.216Z",
        "fk_postulante": 10,
        "propiedad": "EMAIL"
      },
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL"
      },
      {
        "_id": 5,
        "valor": "kevin@hotmail.com",
        "createdAt": "2016-07-22T22:09:15.115Z",
        "updatedAt": "2016-07-22T22:09:15.115Z",
        "fk_postulante": 2,
        "propiedad": "EMAIL"
      }
    ]
  }

Parámetros opcionales de la URL
-------------------------------

Muestra una determinada cantidad de datos de los Postulantes
````````````````````````````````````````````````````````````

**URL**
::

  /datosPostulantes?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /datosPostulantes?elementos=3

**Respuesta:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 2,
        "valor": "daniel@daniel.com",
        "createdAt": "2016-02-14T22:03:48.216Z",
        "updatedAt": "2016-02-14T22:03:48.216Z",
        "fk_postulante": 1,
        "propiedad": "EMAIL"
      },
      {
        "_id": 3,
        "valor": "abc@gmail.com",
        "createdAt": "2016-03-16T22:08:55.216Z",
        "updatedAt": "2016-03-16T22:08:55.216Z",
        "fk_postulante": 10,
        "propiedad": "EMAIL"
      },
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL"
      }
    ]
  }

Muestra los datos de cada Postulante en una determinada página
``````````````````````````````````````````````````````````````

**URL**
::

  /datosPostulantes?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /datosPostulantes?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 2,
        "valor": "daniel@daniel.com",
        "createdAt": "2016-02-14T22:03:48.216Z",
        "updatedAt": "2016-02-14T22:03:48.216Z",
        "fk_postulante": 1,
        "propiedad": "EMAIL"
      },
      {
        "_id": 3,
        "valor": "abc@gmail.com",
        "createdAt": "2016-03-16T22:08:55.216Z",
        "updatedAt": "2016-03-16T22:08:55.216Z",
        "fk_postulante": 10,
        "propiedad": "EMAIL"
      },
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL"
      },
      {
        "_id": 5,
        "valor": "kevin@hotmail.com",
        "createdAt": "2016-07-22T22:09:15.115Z",
        "updatedAt": "2016-07-22T22:09:15.115Z",
        "fk_postulante": 2,
        "propiedad": "EMAIL"
      }
    ]
  }

Ordena los datos de los Postulantes ascendentemente en base a un campo del modelo
`````````````````````````````````````````````````````````````````````````````````

**URL**
::

  /datosPostulantes?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /datosPostulantes?ordenarPor=valor

**Respuesta:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 3,
        "valor": "abc@gmail.com",
        "createdAt": "2016-03-16T22:08:55.216Z",
        "updatedAt": "2016-03-16T22:08:55.216Z",
        "fk_postulante": 10,
        "propiedad": "EMAIL"
      },
      {
        "_id": 2,
        "valor": "daniel@daniel.com",
        "createdAt": "2016-02-14T22:03:48.216Z",
        "updatedAt": "2016-02-14T22:03:48.216Z",
        "fk_postulante": 1,
        "propiedad": "EMAIL"
      },
      {
        "_id": 5,
        "valor": "kevin@hotmail.com",
        "createdAt": "2016-07-22T22:09:15.115Z",
        "updatedAt": "2016-07-22T22:09:15.115Z",
        "fk_postulante": 2,
        "propiedad": "EMAIL"
      },
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL"
      }
    ]
  }

Ordena los datos de los Postulantes descendentemente en base a un campo del modelo
``````````````````````````````````````````````````````````````````````````````````

**URL**
::

  /datosPostulantes?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /datosPostulantes?ordenarPor=valor&orden=true

**Respuesta:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL"
      },
      {
        "_id": 5,
        "valor": "kevin@hotmail.com",
        "createdAt": "2016-07-22T22:09:15.115Z",
        "updatedAt": "2016-07-22T22:09:15.115Z",
        "fk_postulante": 2,
        "propiedad": "EMAIL"
      },
      {
        "_id": 2,
        "valor": "daniel@daniel.com",
        "createdAt": "2016-02-14T22:03:48.216Z",
        "updatedAt": "2016-02-14T22:03:48.216Z",
        "fk_postulante": 1,
        "propiedad": "EMAIL"
      },
      {
        "_id": 3,
        "valor": "abc@gmail.com",
        "createdAt": "2016-03-16T22:08:55.216Z",
        "updatedAt": "2016-03-16T22:08:55.216Z",
        "fk_postulante": 10,
        "propiedad": "EMAIL"
      }
    ]
  }

Busca los postulantes que en el atributo "valor" contenga la palabra "adsib"
````````````````````````````````````````````````````````````````````````````

**URL**
::

  /datosPostulantes?en=valor1&palabras=valor2

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo:**
::

  /datosPostulantes?en=valor&palabras=adsib

**Respuesta:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL"
      }
    ]
  }

Muestra el detalle de Postulante y Tipos de dato del correo "palvarez@adsib.gob.bo"
```````````````````````````````````````````````````````````````````````````````````

**URL**
::

  /datosPostulantes?en=valor1&incluye=%7B%22entidad%22:%22valor2%22%7D&incluye=%7B%22entidad%22:%22valor3%22%7D&palabras=valor4

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
`palabra`     String (array)  Cada elemento del array contiene `palabra` que representa el criterio a buscar.
============  ==============  ================================================================================

**Ejemplo:**
::

  /datosPostulantes?en=valor&incluye=%7B%22entidad%22:%22Postulante%22%7D&incluye=%7B%22entidad%22:%22TipoDato%22%7D&palabras=palvarez@adsib.gob.bo

**Respuesta:**

.. code:: json

  {
    "count":4,"rows":
    [
      {
        "_id": 4,
        "valor": "palvarez@adsib.gob.bo",
        "createdAt": "2016-04-18T22:01:28.110Z",
        "updatedAt": "2016-04-18T22:01:28.110Z",
        "fk_postulante": 5,
        "propiedad": "EMAIL",
        "Postulante":
        {
          "_id": 5,
          "nombres": "Mauricio",
          "apellidos": "Pinedo Reyes",
          "ci": 98381323,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        },
        "TipoDato":
        {
          "propiedad": "EMAIL",
          "nombre": "correo electronico",
          "createdAt": "2016-03-14T22:52:31.554Z",
          "updatedAt": "2016-03-14T22:52:31.554Z"
        }
      }
    ]
  }

Respuesta de error
------------------

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
-------------
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 datos de postulantes por defecto
- Solo se puede hacer "incluye" con las entidades "tipo_dato" y "postulante"

----

Crear registro para Dato de Postulante
======================================

URL
---
::

  /datosPostulantes

Método
------
``POST``

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
      "valor":"email@email.net",
      "propiedad":"EMAIL",
      "fk_postulante":"1"
  }

Respuesta correcta
------------------

**Código:** 201

**Contenido:**

.. code:: json

  {
    "_id": 3,
    "valor": "email@email.net",
    "propiedad": "EMAIL",
    "fk_postulante": 1,
    "updatedAt": "2016-03-15T22:59:34.162Z",
    "createdAt": "2016-03-15T22:59:34.162Z"
  }

Respuesta de error
------------------

**Código:** 400

**Contenido:**

.. code:: json

  {
   "name": "SequelizeValidationError",
   "message": "Validation error: Por favor ingrese un comentario",
   "errors": [
     {
       "message": "Por favor ingrese un comentario",
       "type": "Validation error",
       "path": "contenido",
       "value": {},
       "__raw": {}
     }
   ]
  }

----

Mostrar un Dato para postulante
===============================

URL
---
::

  /datosPostulantes/:id

Método
------
``GET``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=2

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
   "_id": 2,
   "valor": "daniel@daniel.com",
   "createdAt": "2016-02-14T22:03:48.216Z",
   "updatedAt": "2016-02-14T22:03:48.216Z",
   "fk_postulante": 1,
   "propiedad": "EMAIL"
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
   "message": "Entidad no encontrada"
  }

----

Editar datos Postulante
=======================

URL
---
::

  /datosPostulantes/

Método
------
``PUT``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=3

Parámetros del body
-------------------
**Ejemplo:**

.. code:: json

  {
    "valor": "email@gmail.com"
  }

Respuesta correcta
------------------

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 3,
    "valor": "email@gmail.com",
    "propiedad": "EMAIL",
    "fk_postulante": 1,
    "updatedAt": "2016-03-15T22:59:34.162Z",
    "createdAt": "2016-03-15T22:59:34.162Z"
  }

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un comentario",
    "errors": [
      {
        "message": "Por favor ingrese un comentario",
        "type": "Validation error",
        "path": "contenido",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar registro de Dato del Postulante
========================================

URL
---
::

  /datosPostulantes/

Método
------
``DELETE``

Parámetros de la URL
--------------------

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=187

Respuesta correcta
------------------

**Código:** 204

Respuesta de error
------------------

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
