Cargo
=====

Obtener todos los cargos
------------------------

URL
```
::

  /cargos

Método
``````
``GET``

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombre": "Encargado de sistemas",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 51,
        "nombre": "Jefe de Unidad",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 52,
        "nombre": "Director Ejecutivo",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 53,
        "nombre": "Consultor",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      }
    ]
  }

Parámetros opcionales de la URL
```````````````````````````````

Muestra una determinada cantidad de cargos
''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /cargos?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**

::

  /cargos?elementos=2

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombre": "Encargado de sistemas",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      }
    ]
  }


Muestra los cargos en una determinada página
''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /cargos?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /cargos?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombre": "Encargado de sistemas",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 51,
        "nombre": "Jefe de Unidad",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 52,
        "nombre": "Director Ejecutivo",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 53,
        "nombre": "Consultor",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      }
    ]
  }


Ordena los cargos ascendentemente en base a un campo del modelo
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /cargos?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo**
::

  /cargos?ordenarPor=nombre

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 53,
        "nombre": "Consultor",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 52,
        "nombre": "Director Ejecutivo",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombre": "Encargado de sistemas",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 51,
        "nombre": "Jefe de Unidad",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      }
    ]
  }


Ordena los cargos descendentemente en base a un campo del modelo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /cargos?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /cargos?ordenarPor=nombre&orden=true

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 51,
        "nombre": "Jefe de Unidad",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombre": "Encargado de sistemas",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 52,
        "nombre": "Director Ejecutivo",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      },
      {
        "_id": 53,
        "nombre": "Consultor",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      }
    ]
  }


Buscar por "palabra" en "campo" de la entidad cargos
''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /cargos?en=valor1&palabras=valor2

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo**
::

  /cargos?en=nombre&palabras=Secretario

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z"
      }
    ]
  }


Busca los postulantes para el cargo "Secretario"
''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /cargos?en=valor1&incluye=%7B%22entidad%22:%22valor2%22%7D&palabras=valor3

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
`palabra`     String (array)  Cada elemento del array contiene `palabra` que representa el criterio a buscar.
============  ==============  ================================================================================

**Ejemplo**
::

  /cargos?en=nombre&incluye=%7B%22entidad%22:%22Postulantes%22%7D&palabras=Secretario

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 49,
        "nombre": "Secretario",
        "fecha_creacion": "2016-03-14T22:52:31.554Z",
        "fecha_modificacion": "2016-03-14T22:52:31.554Z",
        "Postulantes":
        [
          {
            "_id": 49,
            "nombres": "Victor Hugo",
            "apellidos": "Daza Lima",
            "ci": 372348,
            "fecha_creacion": "2016-01-14T22:52:31.554Z",
            "fecha_modificacion": "2016-01-14T22:52:31.554Z",
            "Postulacion":
            {
              "_id": 1231,
              "presentacion": "Poseo todos los conocimientos necesarios qué la institución requiere",
              "fecha_creacion": "2016-03-16T15:26:38.736Z",
              "fecha_modificacion": "2016-03-16T15:26:38.736Z",
              "fk_postulante": 49,
              "fk_cargo": 49
            }
          },
          {
            "_id": 50,
            "nombres": "Franz Ramiro",
            "apellidos": "Gallardo Portanda",
            "ci": 878789,
            "fecha_creacion": "2016-01-14T22:52:31.554Z",
            "fecha_modificacion": "2016-01-14T22:52:31.554Z",
            "Postulacion":
            {
              "_id": 22,
              "presentacion": "Soy apto para este cargo",
              "fecha_creacion": "2016-03-16T15:26:38.737Z",
              "fecha_modificacion": "2016-03-16T15:26:38.737Z",
              "fk_postulante": 50,
              "fk_cargo": 49
            }
          }
        ]
      }
    ]
  }

Respuesta de error
``````````````````

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }


Observaciones
`````````````
- Esta petición cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 cargos por defecto

----

Crear un cargo
--------------

URL
```
::

  /cargos

Método
``````
``POST``

Parámetros del body
```````````````````
**Ejemplo**

.. code:: json

  {
      "nombre":"Jefe"
  }


Respuesta correcta
``````````````````

**Código:** 201

**Contenido:**

.. code:: json

  {
    "_id": 81,
    "nombre": "Jefe",
    "fecha_modificacion": "2016-03-15T22:26:13.065Z",
    "fecha_creacion": "2016-03-15T22:26:13.065Z"
  }


Respuesta de error
``````````````````

**Código:** 400

***Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Ingrese un nombre para el cargo",
    "errors": [
      {
        "message": "Ingrese un nombre para el cargo",
        "type": "Validation error",
        "path": "nombre",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Mostrar un cargo
----------------
URL
```
::

  /cargos/:id

Método
``````
``GET``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo**
::

  id=49

Respuesta correcta
``````````````````

**Código:** 200

**Ejemplo**

.. code:: json

  {
    "_id": 49,
    "nombre": "Secretario",
    "fecha_creacion": "2016-03-14T22:52:31.554Z",
    "fecha_modificacion": "2016-03-14T22:52:31.554Z"
  }


Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar un cargo
---------------
URL
```
::

  /cargos/:id

Método
``````
``PUT``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo**
::

  id=83

Parámetros del body
```````````````````

**Ejemplo**

.. code:: json

  {
      "nombre":"Jefe modificado"
  }


Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 83,
    "nombre": "Jefe modificado",
    "fecha_creacion": "2016-03-15T22:28:25.816Z",
    "fecha_modificacion": "2016-03-15T22:29:50.184Z"
  }


Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Ingrese un nombre para el cargo",
    "errors": [
      {
        "message": "Ingrese un nombre para el cargo",
        "type": "Validation error",
        "path": "nombre",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar un cargo
-----------------
URL
```
::

  /cargos/:id

Método
``````
``DELETE``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo**
::

  id=83

Respuesta correcta
``````````````````

**Código:** 204

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
