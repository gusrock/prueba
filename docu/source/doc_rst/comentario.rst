Comentario
==========

Mostrar todos los comentarios
-----------------------------

URL
```
::

  /comentarios

Método
``````
``GET``

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 178,
        "contenido": "¿A qué hora es el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.265Z",
        "fecha_modificacion": "2016-03-16T15:40:22.265Z",
        "fk_postulante": 241
      },
      {
        "_id": 179,
        "contenido": "¿Es necesario llevar carnet?",
        "fecha_creacion": "2016-03-16T15:40:22.266Z",
        "fecha_modificacion": "2016-03-16T15:40:22.266Z",
        "fk_postulante": 241
      },
      {
        "_id": 180,
        "contenido": "¿Por qué se postergo el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.267Z",
        "fecha_modificacion": "2016-03-16T15:40:22.267Z",
        "fk_postulante": 242
      }
    ]
  }

Parámetros opcionales de la URL
```````````````````````````````

Muestra una determinada cantidad de comentarios
'''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /comentarios?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /comentarios?elementos=2

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 178,
        "contenido": "¿A qué hora es el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.265Z",
        "fecha_modificacion": "2016-03-16T15:40:22.265Z",
        "fk_postulante": 241
      },
      {
        "_id": 179,
        "contenido": "¿Es necesario llevar carnet?",
        "fecha_creacion": "2016-03-16T15:40:22.266Z",
        "fecha_modificacion": "2016-03-16T15:40:22.266Z",
        "fk_postulante": 241
      }
    ]
  }

Muestra los comentarios en una determinada página
'''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /comentarios?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /comentarios?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 178,
        "contenido": "¿A qué hora es el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.265Z",
        "fecha_modificacion": "2016-03-16T15:40:22.265Z",
        "fk_postulante": 241
      },
      {
        "_id": 179,
        "contenido": "¿Es necesario llevar carnet?",
        "fecha_creacion": "2016-03-16T15:40:22.266Z",
        "fecha_modificacion": "2016-03-16T15:40:22.266Z",
        "fk_postulante": 241
      },
      {
        "_id": 180,
        "contenido": "¿Por qué se postergo el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.267Z",
        "fecha_modificacion": "2016-03-16T15:40:22.267Z",
        "fk_postulante": 242
      }
    ]
  }

Ordena los comentarios ascendentemente en base a un campo del modelo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /comentarios?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /comentarios?ordenarPor=contenido

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 178,
        "contenido": "¿A qué hora es el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.265Z",
        "fecha_modificacion": "2016-03-16T15:40:22.265Z",
        "fk_postulante": 241
      },
      {
        "_id": 179,
        "contenido": "¿Es necesario llevar carnet?",
        "fecha_creacion": "2016-03-16T15:40:22.266Z",
        "fecha_modificacion": "2016-03-16T15:40:22.266Z",
        "fk_postulante": 241
      },
      {
        "_id": 180,
        "contenido": "¿Por qué se postergo el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.267Z",
        "fecha_modificacion": "2016-03-16T15:40:22.267Z",
        "fk_postulante": 242
      }
    ]
  }

Ordena los comentarios descendentemente en base a un campo del modelo
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /comentarios?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /comentarios?ordenarPor=contenido&orden=true

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 180,
        "contenido": "¿Por qué se postergo el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.267Z",
        "fecha_modificacion": "2016-03-16T15:40:22.267Z",
        "fk_postulante": 242
      },
      {
        "_id": 179,
        "contenido": "¿Es necesario llevar carnet?",
        "fecha_creacion": "2016-03-16T15:40:22.266Z",
        "fecha_modificacion": "2016-03-16T15:40:22.266Z",
        "fk_postulante": 241
      },
      {
        "_id": 178,
        "contenido": "¿A qué hora es el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.265Z",
        "fecha_modificacion": "2016-03-16T15:40:22.265Z",
        "fk_postulante": 241
      }
    ]
  }

Busca los comentarios que contenga la palabra "postergo" en el campo "contenido"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /comentarios?en=valor1&palabras=valor2

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo:**
::

  /comentarios?en=contenido&palabras=postergo

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 180,
        "contenido": "¿Por qué se postergo el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.267Z",
        "fecha_modificacion": "2016-03-16T15:40:22.267Z",
        "fk_postulante": 242
      }
    ]
  }

Muestra los postulantes de cada comentario
''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /comentarios?incluye=%7B%22entidad%22:%22valor1%22%7D

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
============  ==============  ================================================================================

**Ejemplo:**
::

  /comentarios?incluye=%7B%22entidad%22:%22Postulante%22%7D

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 178,
        "contenido": "¿A qué hora es el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.265Z",
        "fecha_modificacion": "2016-03-16T15:40:22.265Z",
        "fk_postulante": 241,
        "Postulante":
        {
          "_id": 241,
          "nombres": "Hugo Juan",
          "apellidos": "Layme Pereida",
          "ci": 372348,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        }
      },
      {
        "_id": 179,
        "contenido": "¿Es necesario llevar carnet?",
        "fecha_creacion": "2016-03-16T15:40:22.266Z",
        "fecha_modificacion": "2016-03-16T15:40:22.266Z",
        "fk_postulante": 241,
        "Postulante":
        {
          "_id": 241,
          "nombres": "Hugo Juan",
          "apellidos": "Layme Pereida",
          "ci": 372348,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        }
      },
      {
        "_id": 180,
        "contenido": "¿Por qué se postergo el examen?",
        "fecha_creacion": "2016-03-16T15:40:22.267Z",
        "fecha_modificacion": "2016-03-16T15:40:22.267Z",
        "fk_postulante": 242,
        "Postulante":
        {
          "_id": 242,
          "nombres": "Juan Pablo",
          "apellidos": "Oropeza Velez",
          "ci": 233232,
          "fecha_creacion": "2016-01-14T22:52:31.554Z",
          "fecha_modificacion": "2016-01-14T22:52:31.554Z"
        }
      }
    ]
  }

Respuesta de error
``````````````````

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
`````````````
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 comentarios por defecto

----

Crear un comentario
-------------------

URL
```
::

  /comentarios

Método
``````
``POST``

Parámetros del body
```````````````````
**Ejemplo:**

.. code:: json

  {
      "contenido": "comentario de prueba",
      "fk_postulante":241
  }

Respuesta correcta
``````````````````

**Código:** 201

**Contenido:**

.. code:: json

  {
    "_id": 189,
    "contenido": "comentario de prueba",
    "fk_postulante": 241,
    "fecha_modificacion": "2016-03-16T15:47:04.495Z",
    "fecha_creacion": "2016-03-16T15:47:04.495Z"
  }


Respuesta de error
``````````````````

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un comentario",
    "errors": [
      {
        "message": "Por favor ingrese un comentario",
        "type": "Validation error",
        "path": "contenido",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Mostrar un comentario
---------------------

URL
```
::

  /cargos/:id

Método
``````
``GET``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=49

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 180,
    "contenido": "¿Por qué se postergo el examen?",
    "fecha_creacion": "2016-03-16T15:40:22.267Z",
    "fecha_modificacion": "2016-03-16T15:40:22.267Z",
    "fk_postulante": 242
  }

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar un comentario
--------------------

URL
```
::

  /comentarios/:id

Método
``````
``PUT``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Parámetros del body
```````````````````
**Ejemplo:**

.. code:: json

  {
      "contenido": "Comentario modificado",
      "fk_postulante": 241
  }

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 83,
    "contenido": "Comentario modificado",
    "fecha_creacion": "2016-03-16T15:40:22.265Z",
    "fecha_modificacion": "2016-03-16T15:48:49.656Z",
    "fk_postulante": 241
  }

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }


**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un comentario",
    "errors": [
      {
        "message": "Por favor ingrese un comentario",
        "type": "Validation error",
        "path": "contenido",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar un comentario
----------------------

URL
```
::

  /comentarios/:id

Método
``````
``DELETE``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
``````````````````

**Código:** 204

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Obtener todos los comentarios de un postulante
----------------------------------------------

URL
```
::

  /comentarios/postulate/:id

Método
``````
``GET``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  [
    {
      "_id": 190,
      "contenido": "¿A qué hora es el examen?",
      "fecha_creacion": "2016-03-16T15:50:28.624Z",
      "fecha_modificacion": "2016-03-16T15:50:28.624Z",
      "fk_postulante": 246
    },
    {
      "_id": 191,
      "contenido": "¿Es necesario llevar carnet?",
      "fecha_creacion": "2016-03-16T15:50:28.625Z",
      "fecha_modificacion": "2016-03-16T15:50:28.625Z",
      "fk_postulante": 246
    }
  ]

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Obtener todos los postulantes a un cargo
----------------------------------------

URL
```
::

  /postulaciones/cargo/:id

Método
``````
``GET``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=286

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  [
    {
      "_id": 241,
      "nombres": "Juan Carlos",
      "apellidos": "Perez Gomez",
      "ci": "6867562",
      "createdAt": "2016-03-16T15:40:22.195Z",
      "updatedAt": "2016-03-16T15:40:22.195Z",
      "Postulacion": {
        "_id": 28,
        "presentacion": "Texto de presentacion1",
        "fecha_creacion": "2016-03-16T15:40:22.382Z",
        "fecha_modificacion": "2016-03-16T15:40:22.382Z",
        "fk_postulante": 241,
        "fk_cargo": 286
      }
    },
    {
      "_id": 242,
      "nombres": "Ana Pamela",
      "apellidos": "Rodriguez",
      "ci": "6867122",
      "createdAt": "2016-03-16T15:40:22.195Z",
      "updatedAt": "2016-03-16T15:40:22.195Z",
      "Postulacion": {
        "_id": 29,
        "presentacion": "Texto de presentacion1",
        "fecha_creacion": "2016-03-16T15:40:22.382Z",
        "fecha_modificacion": "2016-03-16T15:40:22.382Z",
        "fk_postulante": 242,
        "fk_cargo": 286
      }
    }
  ]

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
