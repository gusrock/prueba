Postulante
==========

Mostrar todos los Postulantes
-----------------------------

URL
```
::

  /postulantes

Método
``````
``GET``

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombres": "Mariela Pamela",
        "apellidos": "Borja Jimenez",
        "ci": 150195,
        "fecha_creacion": "2016-02-14T10:45:31.554Z",
        "fecha_modificacion": "2016-02-14T10:45:31.554Z"
      },
      {
        "_id": 51,
        "nombres": "Alan",
        "apellidos": "Aparicio Gonzales",
        "ci": 572348,
        "fecha_creacion": "2016-03-14T21:52:54.554Z",
        "fecha_modificacion": "2016-03-14T21:52:54.554Z"
      }
    ]
  }

Parámetros opcionales de la URL
```````````````````````````````

Muestra una determinada cantidad de postulantes
'''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /postulantes?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /postulantes?elementos=1

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z"
      }
    ]
  }

Muestra los postulantes en una determinada página
'''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /postulantes?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /postulantes?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombres": "Mariela Pamela",
        "apellidos": "Borja Jimenez",
        "ci": 150195,
        "fecha_creacion": "2016-02-14T10:45:31.554Z",
        "fecha_modificacion": "2016-02-14T10:45:31.554Z"
      },
      {
        "_id": 51,
        "nombres": "Alan",
        "apellidos": "Aparicio Gonzales",
        "ci": 572348,
        "fecha_creacion": "2016-03-14T21:52:54.554Z",
        "fecha_modificacion": "2016-03-14T21:52:54.554Z"
      }
    ]
  }

Ordena los postulantes ascendentemente en base a un campo del modelo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /postulantes?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /postulantes?ordenarPor=ci

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 50,
        "nombres": "Mariela Pamela",
        "apellidos": "Borja Jimenez",
        "ci": 150195,
        "fecha_creacion": "2016-02-14T10:45:31.554Z",
        "fecha_modificacion": "2016-02-14T10:45:31.554Z"
      },
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z"
      },
      {
        "_id": 51,
        "nombres": "Alan",
        "apellidos": "Aparicio Gonzales",
        "ci": 572348,
        "fecha_creacion": "2016-03-14T21:52:54.554Z",
        "fecha_modificacion": "2016-03-14T21:52:54.554Z"
      }
    ]
  }

Ordena los postulantes descendentemente en base a un campo del modelo
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /postulantes?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /postulantes?ordenarPor=ci&orden=true

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
    	{
        "_id": 51,
        "nombres": "Alan",
        "apellidos": "Aparicio Gonzales",
        "ci": 572348,
        "fecha_creacion": "2016-03-14T21:52:54.554Z",
        "fecha_modificacion": "2016-03-14T21:52:54.554Z"
      },
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z"
      },
      {
        "_id": 50,
        "nombres": "Mariela Pamela",
        "apellidos": "Borja Jimenez",
        "ci": 150195,
        "fecha_creacion": "2016-02-14T10:45:31.554Z",
        "fecha_modificacion": "2016-02-14T10:45:31.554Z"
      }
    ]
  }

Busca los postulantes que en sus nombres y apellidos contengas las palabras "Victor" y "Daza"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /postulantes?en=valor1&en=valor2&palabras=valor3&palabras=valor4

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo:**
::

  /postulantes?en=nombres&en=apellidos&palabras=Victor&palabras=Daza

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z",
      }
    ]
  }

Muestra a los postulante que contenga los Tags PHP y Java
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /postulantes?incluye=%7B%22entidad%22:%22valor1%22,%22ordenarPor%22:%22valor2%22,%22buscar%22:%5B%7B%22palabra%22:%22valor3%22,%22en%22:%22valor4%22%7D,%7B%22palabra%22:%22valor5%22,%22en%22:%22valor6%22%7D%5D%7D

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
`ordenarPor`  String          El nombre del campo correspondiente al modelo de la entidad.
`buscar`      Object (array)  Cada elemento del objeto contiene la `palabra` a buscar.
`palabra`     String (array)  Cada elemento del array contiene `palabra` que representa el criterio a buscar.
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
============  ==============  ================================================================================

**Ejemplo:**
::

  /postulantes?incluye=%7B%22entidad%22:%22Tag%22,%22ordenarPor%22:%22nombre%22,%22buscar%22:%5B%7B%22palabra%22:%22PHP%22,%22en%22:%22nombre%22%7D,%7B%22palabra%22:%22Java%22,%22en%22:%22nombre%22%7D%5D%7D

**Respuesta:**

.. code:: json

  {
    "count":3,"rows":
    [
      {
        "_id": 49,
        "nombres": "Victor Hugo",
        "apellidos": "Daza Lima",
        "ci": 372348,
        "fecha_creacion": "2016-01-14T22:52:31.554Z",
        "fecha_modificacion": "2016-01-14T22:52:31.554Z",
        "Tags":
        [
          {
            "_id": 189,
            "nombre": "Java",
            "fecha_creacion": "2016-03-16T15:50:28.629Z",
            "fecha_modificacion": "2016-03-16T15:50:28.629Z",
            "TagPostulante":
            {
              "_id": 64,
              "fecha_creacion": "2016-03-03T15:50:28.752Z",
              "fecha_modificacion": "2016-03-03T15:50:28.752Z",
              "fk_postulante": 49,
              "fk_tag": 189
            }
          },
          {
            "_id": 188,
            "nombre": "PHP",
            "fecha_creacion": "2016-03-16T15:50:28.629Z",
            "fecha_modificacion": "2016-03-16T15:50:28.629Z",
            "TagPostulante":
            {
              "_id": 64,
              "fecha_creacion": "2016-03-03T15:50:28.752Z",
              "fecha_modificacion": "2016-03-03T15:50:28.752Z",
              "fk_postulante": 49,
              "fk_tag": 188
            }
          }
        ]
      }
    ]
  }

Respuesta de error
``````````````````

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
`````````````
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 postulantes por defecto

----

Crear Postulante
----------------

URL
```
::

  /postulantes

Método
``````
``POST``

Parámetros del body
```````````````````
**Ejemplo:**

.. code:: json

  {
    "nombres": "Victor Hugo",
    "apellidos": "Suarez Perez",
    "ci": "45678912"
  }

Respuesta correcta
``````````````````

**Código:** 201

**Contenido:**

.. code:: json

  {
    "_id": "165",
    "nombres": "Victor Hugo",
    "apellidos": "Suarez Perez",
    "ci": "45678912",
    "fecha_creacion": "2016-01-14T22:52:31.554Z",
    "fecha_modificacion": "2016-01-14T22:52:31.554Z"
  }

Respuesta de error
``````````````````

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese correctamente los datos para el Postulante",
    "errors": [
      {
        "message": "Ingrese Nombre(s) para el postulante.",
        "type": "Validation error",
        "path": "nombres",
        "value": {},
        "__raw": {}
      },
      {
        "message": "Ingrese Apellido(s) para el postulante.",
        "type": "Validation error",
        "path": "apellidos",
        "value": {},
        "__raw": {}
      },
      {
        "message": "Ingrese Cedula de Identidad (CI) para el postulante.",
        "type": "Validation error",
        "path": "ci",
        "value": {},
        "__raw": {}
      }
    ]
  }

**Código:** 409

**Contenido:**

.. code:: json

  {
    "name": "SequelizeUniqueConstraintError",
    "message": "El objeto ya existe",
    "errors": {}
  }

----

Mostrar un Postulante
---------------------

URL
```
::

  /postulantes/:id

Método
``````
``GET``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=49

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 49,
    "nombres": "Victor Hugo",
    "apellidos": "Daza Lima",
    "ci": 372348,
    "fecha_creacion": "2016-01-14T22:52:31.554Z",
    "fecha_modificacion": "2016-01-14T22:52:31.554Z"
  }

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar Postulante
-----------------

URL
```
::

  /postulantes/

Método
``````
``PUT``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=165

Parámetros del body
```````````````````
**Ejemplo:**

.. code:: json

  {
    "nombres": "Victor Hugo",
    "apellidos": "Suarez Pereida",
    "ci": "45678912",
  }

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 165,
    "nombres": "Victor Hugo",
    "apellidos": "Suarez Pereida",
    "ci": "45678912",
    "fecha_creacion": "2016-01-14T22:52:31.554Z",
    "fecha_modificacion": "2016-01-14T23:45:11.554Z"
  }

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un mensaje de presentacion",
    "errors": [
      {
        "message": "Por favor ingrese un mensaje de presentacion",
        "type": "Validation error",
        "path": "presentacion",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar Postulante
-------------------

URL
```
::

  /postulantes/

Método
``````
``DELETE``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=165

Respuesta correcta
``````````````````

**Código:** 204

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
   "message": "Entidad no encontrada"
  }
