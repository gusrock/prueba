Servicio BACKEND para migrar datos de Moodle
============================================

El presente servicio realiza la migración de todos los datos del postulante (usuario).

Prerrequisitos de configuración
-------------------------------

**Tablas de extracción de Datos [mdl_user, mdl_tag_instance, mdl_tag]**

Tabla en Moodle : **mdl_user**

==============  ==========  ================================================================================
**Nombre**      **Tipo**    **Descripción**
==============  ==========  ================================================================================
`firstname`     String      Representa nombre(s) del postulante.
`lastname`      String      Representa apellido(s) del postulante.
`email`         String      Representa correo electronico del postulante.
`username`      String      Representa Cedula de Identidad del postulante.
`phone1`        String      Representa número de telefono(s) fijo del postulante.
`phone2`        String      Representa número de telefono(s) celular del postulante.
`address`       String      Representa la dirección del postulante.
`city`          String      Representa la ciudad del postulante.
`country`       String      Representa el país del postulante.
`url`           String      Representa dirección web de la hoja de vida(curriculum vitae) del postulante.
==============  ==========  ================================================================================

Tabla en Moodle : **mdl_tag_instance** y **mdl_tag**

==========================  ==========  ================================================================================
**Nombre**                  **Tipo**    **Descripción**
==========================  ==========  ================================================================================
`mdl_tag_instance.itemid`   String      Representa el ID del usuario Moodle (idUsuarioMoodle).
`mdl_tag.name`              String      Representa el NOMBRE del tag Moodle (nombreTag).
==========================  ==========  ================================================================================

**Realizar cambios en archivo:** .../server/config/environment/development.js

======================  ==========  ==================================================================
**Variable**            **Valor**   **Descripción**
======================  ==========  ==================================================================
`usuarioMoodle`         root        Usuario MySQL del origen de datos de Moodle.
`passwordMoodle`        admin       Contraseña del Usuario de Base de Datos MySQL de Moodle.
`hostMoodle`            localhost   Dirección web (IP) donde se encuentra la Base de Datos de Moodle.
`baseDatosMoodle`       moodle      Nombre de la Base de Datos Moodle.
======================  ==========  ==================================================================

URL
---
::

    /api/migracionMoodle/migrarDatosMoodle

Metodo
------
::

    GET

Respuesta correcta
------------------

**Codigo:** 200

**Ejemplo**

.. code:: json

    {
      "titulo": "Total de registros migrados de Moodle",
      "postulantes": 1190,
      "telefonoFijo": 21,
      "telefonoCelular": 1055,
      "Direccion": 13,
      "Ciudad": 238,
      "Pais": 272,
      "email": 685,
      "cv": 685,
      "totalDatosPostulante": 2969,
      "tag": 167,
      "tagPostulante": 3272
    }

Respuesta de error
------------------

**Codigo:** 500

**Contenido:**

.. code:: json

    {
        "error":"Ocurrio un problema inesperado en el servidor"
    }

Observaciones
-------------
- Las tablas del modelo(involucradas) se vaciaran, para realizar la migración de toda la información de Moodle(Mysql) a sistema de gestion postulante(PostgrSQL).
